#!/bin/python3

import requests
import argparse
import yaml
from lxml import etree
from urllib.parse import urlparse
from feedgen.feed import FeedGenerator
import getpass
import os
from datetime import date


ATOM_NS = '{http://www.w3.org/2005/Atom}'
# parse arguments
parser = argparse.ArgumentParser(description='Write atom feed from comics')
parser.add_argument('--dest', default='.',
                    help='Directory to store atom feeds file')
parser.add_argument('--comics', default='comics.yml',
                    help='Comics database file')
parser.add_argument('--debug', action="store_true")
parser.add_argument('--image', action="store_true")
parser.add_argument('comic', nargs='?')
args = parser.parse_args()


def get_img(comic):
    h = {'User-Agent': "plop"}
    r = requests.get(comic['url'],headers=h)
    xp = comic['xpaths']
    for x in xp[:-1]:
        match = etree.HTML(r.text).xpath(x)
        if len(match) < 1:
            print("xpath:", x)
            print("text:", r.text)
            raise Exception
        r = requests.get(match[0].attrib['href'])

    match = etree.HTML(r.text).xpath(xp[-1])
    url = match[0].attrib['src']
    if urlparse(url).hostname is None:
        url = comic['url'] + url
    return url


def create_atom_file(comic, name, img, atom_file):
    url = comic['url']
    fg = FeedGenerator()
    fg.author({'name': getpass.getuser()})
    fg.title('Atom feed for {}'.format(name))
    # md5sum de l'image ?
    fg.id(url)

    fe = fg.add_entry()
    fe.title("Entry for {}".format(date.today().isoformat()))
    fe.id(img)
    fe.content(content="<img src='{}' />".format(img), type='xhtml')

    fg.atom_file(atom_file)


def get_entry_id(element):
    return element.find(ATOM_NS + 'id').text


def append_atom_file(comic, name, img, atom_file):
    # ugly, need to cleanup
    atom_file_tmp = atom_file + '.tmp'
    create_atom_file(comic, name, img, atom_file_tmp)
    atom_old = etree.parse(atom_file).getroot()
    atom_new = etree.parse(atom_file_tmp).getroot()

    for i in atom_new.findall(ATOM_NS + "entry"):
        dup = False
        for j in atom_old.findall(ATOM_NS + "entry"):
            if get_entry_id(j) == get_entry_id(i):
                dup = True
        if not dup:
            atom_old.insert(0, i)

    # TODO add cleanup of old entries ( n>X, avec X configurable)
    open(atom_file, "wb").write(etree.tostring(atom_old))


def add_comic_to_atom(c, comics):
    i = get_img(comics[c])
    f = os.path.join(args.dest, c + '.xml')
    if not os.path.exists(f):
        create_atom_file(comics[c], c, i, f)
    else:
        append_atom_file(comics[c], c, i, f)


comics = yaml.safe_load(open(args.comics).read())

if args.comic is not None and args.image:
    print(get_img(comics[args.comic]))

if args.comic is not None:
    c = args.comic
    add_comic_to_atom(c, comics)
else:
    for c in comics:
        if args.debug:
            print(c)
        add_comic_to_atom(c, comics)
